﻿using System;
using System.Threading.Tasks;
using ConfigInjector.QuickAndDirty;
using Serilog;
using Serilog.Core;
using InstaImageApi.Configuration;
using Serilog.Events;
using SerilogWeb.Classic;
using SerilogWeb.Classic.Enrichers;

namespace InstaImageApi
{
    public static class WireUpLogging
    {
        public static void Bootstrap(Func<LoggerConfiguration, LoggerConfiguration> applyAdditionalConfiguration = null)
        {
            var logEventLevel = DefaultSettingsReader.Get<LogLevel>();
            var loglevelSwitch = new LoggingLevelSwitch(logEventLevel);
            var appPoolId = Environment.GetEnvironmentVariable("APP_POOL_ID", EnvironmentVariableTarget.Process);
            ApplicationLifecycleModule.RequestLoggingLevel = LogEventLevel.Verbose;

            var loggerConfiguration =
                new LoggerConfiguration().MinimumLevel.ControlledBy(loglevelSwitch).WriteTo.LiterateConsole();
            var seqServerUrl = DefaultSettingsReader.Get<SeqServerUrl>();

            if (!string.IsNullOrEmpty(seqServerUrl))
            {
                var seqApiKey = DefaultSettingsReader.Get<SeqApiKey>();
                loggerConfiguration
                    .WriteTo.Seq(seqServerUrl.ToString(), apiKey: seqApiKey, controlLevelSwitch: loglevelSwitch,
                        compact: true)
                    .Enrich.With<HttpRequestClientHostIPEnricher>()
                    .Enrich.With<HttpRequestIdEnricher>()
                    .Enrich.With<HttpRequestNumberEnricher>()
                    .Enrich.With<HttpRequestTypeEnricher>()
                    .Enrich.With<HttpRequestRawUrlEnricher>()
                    .Enrich.With<HttpRequestUrlReferrerEnricher>()
                    .Enrich.With<HttpRequestUserAgentEnricher>()
                    .Enrich.With<UserNameEnricher>();
            }
            if (applyAdditionalConfiguration != null)
                loggerConfiguration = applyAdditionalConfiguration(loggerConfiguration);
            Log.Logger = loggerConfiguration.CreateLogger();
            AppDomain.CurrentDomain.DomainUnload += OnDomainUnload;
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
            TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;
            Log.Information("Logger online");
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                var logEventLevel = e.IsTerminating ? LogEventLevel.Fatal : LogEventLevel.Error;

                var ex = e.ExceptionObject as Exception;
                var message = ex?.Message ?? "(No exception provided to handler.)";
                Log.Write(logEventLevel, "Unhandled exception: {Message}", message);
            }
            catch
            {
                // Uh oh... something has gone badly wrong. Hopefully it will get written
                // down somewhere else because we can't do it here...
            }
        }

        private static void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            var logEventLevel = e.Observed ? LogEventLevel.Error : LogEventLevel.Fatal;
            Log.Write(logEventLevel, e.Exception, "Unobserved task exception: {Message}", e.Exception.Message);
        }

        private static void OnDomainUnload(object sender, EventArgs e)
        {
            Log.Information("Application domain unloading");
            Log.CloseAndFlush();
        }
    }
}