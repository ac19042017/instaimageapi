﻿using System;
using System.Collections.Generic;

namespace InstaImageApi.Repository
{
    public class Repository<T1, T2> : IRepository<T1, T2> 
    {
        public void Create(T1 newItem)
        {
            throw new NotImplementedException();
        }

        public T1 Read(T2 tkey)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T1> ReadAll()
        {
            throw new NotImplementedException();
        }

        public T1 Update(T2 tkey, T1 updateItem)
        {
            throw new NotImplementedException();
        }

        public void Delete(T2 tkey)
        {
            throw new NotImplementedException();
        }
    }
}
