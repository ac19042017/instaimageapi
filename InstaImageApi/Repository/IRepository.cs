﻿using System.Collections.Generic;

namespace InstaImageApi.Repository
{
    public interface IRepository<T,in TKey>
    {
        void Create(T entry);
        T Read(TKey key);
        IEnumerable<T> ReadAll();
        T Update(TKey tkey, T updateItem);
        void Delete(TKey tkey);

    }
}
