﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using InstaImageApi.Database;
using InstaImageApi.Models;

namespace InstaImageApi.Controllers
{
    public class UserPreferencesController : ApiController
    {
        private InstaLikeEntities db;

        public UserPreferencesController(InstaLikeEntities dbContext)
        {
            db = dbContext;
        }

        // GET: api/UserPreferences
        public IQueryable<UserPreferenceModel> GetUserPreferences(string userName)
        {
            return db.UserPreferences.Where(p => p.ActiveInd == true && p.UserName == userName).Select(p =>
            new UserPreferenceModel
            {
                ActiveInd = p.ActiveInd,
                ImageId = p.ImageId,
                LikeStatus = p.LikeStatus,
                UserName = p.UserName
            });
        }

        // PUT: api/UserPreferences/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUserPreference(string id, UserPreferenceModel userPreference)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userPreference.UserName)
            {
                return BadRequest();
            }

            var userPreferencedbEntiy = new UserPreference
            {
                ActiveInd = userPreference.ActiveInd,
                ImageId = userPreference.ImageId,
                LikeStatus = userPreference.LikeStatus,
                UserName = userPreference.UserName
            };

            db.Entry(userPreferencedbEntiy).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserPreferenceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UserPreferences
        [ResponseType(typeof(UserPreferenceModel))]
        public async Task<IHttpActionResult> PostUserPreference(UserPreferenceModel userPreference)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (db.UserPreferences.Count(
                    p =>
                        p.UserName == userPreference.UserName && p.ImageId == userPreference.ImageId &&
                        p.ActiveInd == userPreference.ActiveInd
                        && p.LikeStatus == userPreference.LikeStatus) > 0)
                return Ok();

            var userPreferencedbEntiy = new UserPreference
            {
                ActiveInd = userPreference.ActiveInd,
                ImageId = userPreference.ImageId,
                LikeStatus = userPreference.LikeStatus,
                UserName = userPreference.UserName
            };

            db.UserPreferences.AddOrUpdate(userPreferencedbEntiy);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                if (UserPreferenceExists(userPreference.UserName))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = userPreference.UserName }, userPreference);
        }

        // DELETE: api/UserPreferences/5
        [ResponseType(typeof(UserPreference))]
        public async Task<IHttpActionResult> DeleteUserPreference(string id)
        {
            var userPreference = await db.UserPreferences.FindAsync(id);
            if (userPreference == null)
            {
                return NotFound();
            }

            db.UserPreferences.Remove(userPreference);
            await db.SaveChangesAsync();

            return Ok(userPreference);
        }

        private bool UserPreferenceExists(string id)
        {
            return db.UserPreferences.Count(e => e.UserName == id) > 0;
        }
    }
}