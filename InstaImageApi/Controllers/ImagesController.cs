﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using InstaImageApi.Database;
using InstaImageApi.Models;

namespace InstaImageApi.Controllers
{
    public class ImagesController : ApiController
    {
        private InstaLikeEntities db;

        public ImagesController(InstaLikeEntities dbcontext)
        {
            db = dbcontext;
        }

        // GET: api/Images
        public IQueryable<ImageModel> GetImages()
        {
            var images = db.Images.Where(img => img.ActiveInd == true).Select(img => new ImageModel
            {
                ImageUrl = img.ImageUrl,
                ActiveInd = img.ActiveInd,
                ImageId = img.ImageId
            });
            return images;
        }

        // GET: api/Images/5
        [ResponseType(typeof(ImageModel))]
        public async Task<IHttpActionResult> GetImage(string id)
        {
            var image = await db.Images.FindAsync(id);
            if (image == null)
            {
                return NotFound();
            }

            var imagemodel = new ImageModel
            {
                ActiveInd = image.ActiveInd,
                ImageId = image.ImageId,
                ImageUrl = image.ImageUrl
            };

            return Ok(imagemodel);
        }


        [Route("api/RandomImage")]
        [ResponseType(typeof(ImageModel))]
        public async Task<IHttpActionResult> GetRandomImage()
        {
            var totalCount = await db.Images.CountAsync(img => img.ActiveInd == true);
            var rand = new Random();
            var randomImageIdx = rand.Next(1, totalCount);
            var images = await db.Images.Where(img => img.ActiveInd == true).Select(img => new ImageModel
            {
                ImageUrl = img.ImageUrl,
                ActiveInd = img.ActiveInd,
                ImageId = img.ImageId
            }).ToListAsync();
            
            return Ok(images.ElementAtOrDefault(randomImageIdx));

        }

        // PUT: api/Images/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutImage(string id, ImageModel image)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != image.ImageId)
            {
                return BadRequest();
            }

            var dbImage = new Image
            {
                ActiveInd = image.ActiveInd,
                ImageId = image.ImageId,
                ImageUrl = image.ImageUrl
            };

            db.Entry(dbImage).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Images
        [ResponseType(typeof(ImageModel))]
        public async Task<IHttpActionResult> PostImage(ImageModel image)
        {
            image.ImageId = Guid.NewGuid().ToString();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dbImage = new Image
            {
                ActiveInd = image.ActiveInd,
                ImageId = image.ImageId,
                ImageUrl = image.ImageUrl
            };

            db.Images.Add(dbImage);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ImageExists(image.ImageId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = image.ImageId }, image);
        }

        // DELETE: api/Images/5
        [ResponseType(typeof(Image))]
        public async Task<IHttpActionResult> DeleteImage(string id)
        {
            var image = await db.Images.FindAsync(id);
            if (image == null)
            {
                return NotFound();
            }

            db.Images.Remove(image);
            await db.SaveChangesAsync();

            return Ok(image);
        }

        private bool ImageExists(string id)
        {
            return db.Images.Count(e => e.ImageId == id) > 0;
        }
    }
}