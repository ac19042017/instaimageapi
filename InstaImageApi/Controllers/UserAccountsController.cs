﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using InstaImageApi.Database;
using InstaImageApi.Models;

namespace InstaImageApi.Controllers
{
    public class UserAccountsController : ApiController
    {
        private InstaLikeEntities db;

        public UserAccountsController(InstaLikeEntities dbContext)
        {
            db = dbContext;
        }

        // GET: api/UserAccounts
        public IQueryable<UserAccountModel> GetUserAccounts()
        {
            return db.UserAccounts.Where(p => p.ActiveInd == true).Select(p =>
                new UserAccountModel
                {
                    ActiveInd = p.ActiveInd,
                    ActiveStatusDateTime = p.ActiveStatusDateTime,
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                    UserName = p.UserName
                });
        }

        // GET: api/UserAccounts/5
        [ResponseType(typeof(UserAccountModel))]
        public async Task<IHttpActionResult> GetUserAccount(string id)
        {
            var userAccount = await db.UserAccounts.FindAsync(id);
            if (userAccount == null)
            {
                return NotFound();
            }

            var userAccountModel = new UserAccountModel
            {
                ActiveInd = userAccount.ActiveInd,
                ActiveStatusDateTime = userAccount.ActiveStatusDateTime,
                FirstName = userAccount.FirstName,
                LastName = userAccount.LastName,
                UserName = userAccount.UserName
            };
            return Ok(userAccountModel);
        }

        // PUT: api/UserAccounts/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUserAccount(string id, UserAccountModel userAccount)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userAccount.UserName)
            {
                return BadRequest();
            }


            var userAccountDbEntity = new UserAccount
            {
                ActiveInd = userAccount.ActiveInd,
                ActiveStatusDateTime = userAccount.ActiveStatusDateTime,
                FirstName = userAccount.FirstName,
                LastName = userAccount.LastName,
                UserName = userAccount.UserName
            };

            db.Entry(userAccountDbEntity).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserAccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UserAccounts
        [ResponseType(typeof(UserAccountModel))]
        public async Task<IHttpActionResult> PostUserAccount(UserAccountModel userAccount)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userAccountDbEntity = new UserAccount
            {
                ActiveInd = userAccount.ActiveInd,
                ActiveStatusDateTime = userAccount.ActiveStatusDateTime,
                FirstName = userAccount.FirstName,
                LastName = userAccount.LastName,
                UserName = userAccount.UserName
            };

            db.UserAccounts.Add(userAccountDbEntity);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UserAccountExists(userAccount.UserName))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = userAccount.UserName }, userAccount);
        }

        // DELETE: api/UserAccounts/5
        [ResponseType(typeof(UserAccount))]
        public async Task<IHttpActionResult> DeleteUserAccount(string id)
        {
            var userAccount = await db.UserAccounts.FindAsync(id);
            if (userAccount == null)
            {
                return NotFound();
            }

            db.UserAccounts.Remove(userAccount);
            await db.SaveChangesAsync();

            return Ok(userAccount);
        }

        private bool UserAccountExists(string id)
        {
            return db.UserAccounts.Count(e => e.UserName == id) > 0;
        }
    }
}