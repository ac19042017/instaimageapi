﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using InstaImageApi.Models;
using Newtonsoft.Json.Linq;

namespace InstaImageApi.Controllers
{
    public class DefaultController : ApiController
    {
        [HttpGet]
        [Route("")]
        public IHttpActionResult Home()
        {
            var apiInfo = new ApiInfo
            {
                Build = GetVersion().ToString(),
                BuildDate = GetBuildDate().ToString(CultureInfo.InvariantCulture),
                CurrentTime = System.DateTime.UtcNow.ToLongDateString(),
                Description = "IMAGING API",
                MachineName = Environment.MachineName
            };

            return Ok(apiInfo);
        }

        private static DateTime GetBuildDate()
        {
            var version = GetVersion();
            var time = new DateTime(2000, 1, 1).AddDays(version.Build).AddSeconds(version.Revision * 2);
            return time;
        }

        private static Version GetVersion()
        {
            return typeof(DefaultController).Assembly.GetName().Version;
        }
    }
}
