﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using Autofac.Builder;
using Serilog;

namespace InstaImageApi
{
    public static class WireUpDi
    {
        public static IContainer WireUpContainer(ContainerBuildOptions containerBuildOptions = ContainerBuildOptions.None)
        {
            var builder = new ContainerBuilder();
            Log.Debug("Scanning {@AssemblyNames} for Autofac modules", AssemblyScanner.AssemblyScanner.MyAssemblies.Select(a => a.GetName().Name).ToArray());
            builder.RegisterAssemblyModules(AssemblyScanner.AssemblyScanner.MyAssemblies);
            return builder.Build(containerBuildOptions);
        }
    }
}