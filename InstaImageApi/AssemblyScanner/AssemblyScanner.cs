﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Serilog;
using ThirdDrawer.Extensions.CollectionExtensionMethods;

namespace InstaImageApi.AssemblyScanner
{
    public static class AssemblyScanner
    {
        private static readonly Lazy<Assembly[]> _myAssemblies = new Lazy<Assembly[]>(ScanAppDomain);
        public static Assembly[] MyAssemblies => _myAssemblies.Value;

        private static Assembly[] ScanAppDomain()
        {
            ForceLoadAllReferencedAssemblies();

            var assemblies = AppDomain.CurrentDomain
                                      .GetAssemblies()
                                      .ToArray();

            return assemblies;
        }


        private static void ForceLoadAllReferencedAssemblies()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var alreadyLoaded = assemblies.Select(a => a.GetName()).ToList();
            ForceLoadAllReferencedAssemblies(assemblies, alreadyLoaded);
        }

        public static void EnsureReferencedAssembliesAreLoaded()
        {
            // ReSharper disable once ReturnValueOfPureMethodIsNotUsed
            MyAssemblies.FirstOrDefault(); // we don't care about the value; we just want to force-load the System.Lazy if it hasn't already been.
        }

        private static void ForceLoadAllReferencedAssemblies(IEnumerable<Assembly> assembliesToTraverse, ICollection<AssemblyName> alreadyLoaded)
        {
            foreach (var assembly in assembliesToTraverse)
            {
                var referencedAssemblies = assembly.GetReferencedAssemblies();

                var referencedAssembliesToLoad = referencedAssemblies
                    .Where(an => alreadyLoaded.None(already => already.FullName == an.FullName))
                    .ToArray();

                var newlyLoadedAssemblies = referencedAssembliesToLoad
                    .Do(alreadyLoaded.Add)
                    .Select(ForceLoad)
                    .NotNull()
                    .ToArray();

                ForceLoadAllReferencedAssemblies(newlyLoadedAssemblies, alreadyLoaded);
            }
        }

        private static Assembly ForceLoad(AssemblyName an)
        {
            try
            {
                var assembly = Assembly.Load(an);
                return assembly;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Force-loading the referenced assembly {AssemblyName} failed", an.FullName);
                return null;
            }
        }
    }
}