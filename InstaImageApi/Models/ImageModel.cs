﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InstaImageApi.Models
{
    public class ImageModel
    {
        public string ImageId { get; set; }

        [Required]
        public string ImageUrl { get; set; }

        [Required]
        public bool ActiveInd { get; set; }
    }
}