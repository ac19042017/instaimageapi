﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InstaImageApi.Models
{
    public class ApiInfo
    {
        public string Build { get; set; }
        public string BuildDate { get; set; }
        public string CurrentTime { get; set; }
        public string Description { get; set; }
        public string MachineName { get; set; }
    }
}