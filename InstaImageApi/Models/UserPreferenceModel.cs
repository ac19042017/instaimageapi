﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InstaImageApi.Models
{
    public class UserPreferenceModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string ImageId { get; set; }

        [Required]
        public bool ActiveInd { get; set; }

        [Required]
        public bool LikeStatus { get; set; }
    }
}