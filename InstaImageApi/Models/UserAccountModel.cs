﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InstaImageApi.Models
{
    public class UserAccountModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public bool ActiveInd { get; set; }

        [Required]
        public System.DateTime ActiveStatusDateTime { get; set; }
    }
}