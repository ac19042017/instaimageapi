﻿using Autofac;
using ConfigInjector.Configuration;

namespace InstaImageApi.AutofacModules
{
    public class ConfigModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            ConfigurationConfigurator.RegisterConfigurationSettings().FromAssemblies(AssemblyScanner.AssemblyScanner.MyAssemblies)
            .RegisterWithContainer(configSetting => builder.RegisterInstance(configSetting)
                                                   .AsSelf()
                                                   .SingleInstance()).DoYourThing();
        }
    }
}