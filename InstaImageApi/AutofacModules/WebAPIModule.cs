﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;

namespace InstaImageApi.AutofacModules
{
    public class WebAPIModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterApiControllers(AssemblyScanner.AssemblyScanner.MyAssemblies);
            builder.RegisterWebApiFilterProvider(GlobalConfiguration.Configuration);
        }
    }
}