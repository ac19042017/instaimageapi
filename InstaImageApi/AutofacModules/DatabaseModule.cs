﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using InstaImageApi.Configuration;
using InstaImageApi.Database;

namespace InstaImageApi.AutofacModules
{
    public class DatabaseModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.Register(c => new InstaLikeEntities(string.Format(c.Resolve<SourceDatabaseContext>().Value, c.Resolve<SourceDatabaseConnection>())))
           .AsSelf()
           .SingleInstance();
        }
    }
}