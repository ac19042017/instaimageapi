﻿using System;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Serilog;

namespace InstaImageApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private IContainer _container;

        protected void Application_Start()
        {
            WireUpLogging.Bootstrap();
            Log.Information("Application starting");
            try
            {
                _container = WireUpDi.WireUpContainer();
                GlobalConfiguration.Configure(WebApiConfig.Register);
                GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(_container);
                Log.Information("Application started");
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "A fatal exception was thrown. The application cannot start.");
                throw;
            }
        }
    }
}
